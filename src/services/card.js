import cardsList from '../fixtures/cardsList';

export default class CardService {
    static init = () => localStorage.setItem('cards', JSON.stringify(cardsList));

    static getAll = () => JSON.parse(localStorage.getItem('cards'));
}
