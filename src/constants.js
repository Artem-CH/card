export const routes = {
    HOME_PAGE: '/',
    ADD_CARD: '/create',
    EDIT_CARD: '/cards/:id',
    EDIT_CARD_ROOT: '/cards'
};
