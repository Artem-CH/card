const cardsList = [
    {
        id: 1,
        title: 'Lorem Ipsum',
        isLike: true,
        description: 'Lorem Ipsum is simply dummy text of the printing and...',
    },
    {
        id: 2,
        title: 'Lorem Ipsum',
        isLike: false,
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    },
    {
        id: 3,
        title: 'Quisque quam ipsum',
        isLike: true,
        description: 'Quisque quam ipsum, faucibus eu odio at, tempus mattis risus.',
    },
    {
        id: 4,
        title: 'Lorem Ipsum',
        isLike: true,
        description: 'Donec venenatis, elit vitae aliquet suscipit, neque magna maximus tortor, nec interdum purus nunc ac magna',
    },
    {
        id: 5,
        title: 'Lorem Ipsum',
        isLike: false,
        description: 'Proin gravida lectus eget magna malesuada pellentesque. ',
    },
    {
        id: 6,
        title: 'Lorem Ipsum',
        isLike: false,
        description: 'Proin gravida lectus eget magna malesuada pellentesque. ',
    }
];

export default cardsList;
