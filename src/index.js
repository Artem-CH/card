import React from 'react';
import { render } from 'react-dom';
import App from './components/app';

import CardService from './services/card';

CardService.init();

render(
    <App />,
    document.getElementById('app')
);
