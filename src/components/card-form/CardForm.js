import React from 'react';
import PropTypes from 'prop-types';

import { Button } from '../common';

const CardForm = (props) => {
    const {
        error,
        title,
        onChangeTitle,
        onChangeDescription,
        description,
        onSubmit,
        children
    } = props;

    return (
        <div className="col-lg-12">
            <div className="card-form">
                <form onSubmit={onSubmit}>
                    <div className="card-form__content">
                        <div>{error}</div>
                        <div className="form-group">
                            <input
                                className="form-control"
                                type="text"
                                value={title}
                                onChange={onChangeTitle}
                                placeholder="Title"
                            />
                        </div>
                        <div className="form-group">
                            <textarea
                                className="form-control"
                                value={description}
                                onChange={onChangeDescription}
                                placeholder="Description"
                                rows="5"
                            />
                        </div>
                    </div>
                </form>
                <div className="d-flex justify-content-between align-items-center">
                    <Button className="btn btn-primary" onClick={onSubmit}>Save</Button>
                    {children}
                </div>
            </div>
        </div>
    );
};

CardForm.propTypes = {
    title: PropTypes.string,
    description: PropTypes.string,
    onChangeTitle: PropTypes.func.isRequired,
    onChangeDescription: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    error: PropTypes.object,
    children: PropTypes.node,
};

export default CardForm;
