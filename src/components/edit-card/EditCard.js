import React, { Component } from 'react';
import PropTypes from 'prop-types';

import CardForm from '../card-form';
import NotFound from '../not-found';
import { Message } from '../common';

import { routes } from '../../constants';
import { isFormEmpty } from '../../helpers';

class EditCard extends Component {
    constructor(props) {
        super(props);
        const card = props.cards.find(c => c.id === +props.match.params.id);
        if (card) {
            this.state = {
                id: card.id,
                title: card.title,
                description: card.description,
                error: '',
            };
        }
    }

    onChangeTitle = (e) => {
        this.setState({
            title: e.target.value,
        });
    };

    onChangeDescription = (e) => {
        this.setState({
            description: e.target.value,
        });
    };

    onDeleteCard = (id) => {
        this.props.onDeleteCard(id);
        this.props.history.push(routes.HOME_PAGE);
    };

    onClose = () => {
        this.setState({ error: '' });
    };

    isFormValid = () => {
        if (isFormEmpty(this.state)) {
            this.setState({ error: 'Fill all fields' });
            return false;
        }

        return true;
    };

    onSubmit = (e) => {
        e.preventDefault();
        if (!this.isFormValid()) {
            return false;
        }

        this.props.onUpdateCard(this.state);
        this.props.history.push(routes.HOME_PAGE);
    };

    render() {
        if (!this.state) {
            return <NotFound />;
        }

        const { id, title, description, error } = this.state;
        const displayError = error ? <Message onClick={this.onClose}>{error}</Message> : null;

        return (
            <CardForm
                title={title}
                description={description}
                onChangeTitle={this.onChangeTitle}
                onChangeDescription={this.onChangeDescription}
                onSubmit={this.onSubmit}
                error={displayError}>
                <a className="btn btn-danger" onClick={() => this.onDeleteCard(id)}>Удалить</a>
            </CardForm>
        );
    }
}

EditCard.propTypes = {
    cards: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        description: PropTypes.string,
    })).isRequired,
    onUpdateCard: PropTypes.func.isRequired,
};

export default EditCard;
