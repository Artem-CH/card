import React, { Component } from 'react';
import PropTypes from 'prop-types';

import CardForm from '../card-form';
import { Message } from '../common';

import { routes } from '../../constants';
import { isFormEmpty } from '../../helpers';

class AddCard extends Component {
    state = {
        title: '',
        description: '',
        error: '',
    };

    onChangeTitle = (e) => {
        this.setState({
            title: e.target.value,
        });
    };

    onChangeDescription = (e) => {
        this.setState({
            description: e.target.value,
        });
    };

    onClose = () => {
        this.setState({ error: '' });
    };

    isFormValid = () => {
        if (isFormEmpty(this.state)) {
            this.setState({ error: 'Fill all fields' });
            return false;
        }

        return true;
    };

    onSubmit = (e) => {
        e.preventDefault();
        if (!this.isFormValid()) {
            return false;
        }

        this.props.onAddCard(this.state);
        this.props.history.push(routes.HOME_PAGE);
    };

    render() {
        const { title, description, error } = this.state;
        const displayError = error ? <Message onClick={this.onClose}>{error}</Message> : null;

        return (
            <CardForm
                title={title}
                description={description}
                onChangeTitle={this.onChangeTitle}
                onChangeDescription={this.onChangeDescription}
                onSubmit={this.onSubmit}
                error={displayError} />
        );
    }
}

AddCard.propTypes = {
    onAddCard: PropTypes.func.isRequired,
};

export default AddCard;
