import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { routes } from '../../constants';

const CardList = ({ cards, onLike }) => {
    const cardsList = cards.map((card) => {
        const isLike = card.isLike ? 'btn-danger' : 'btn-outline-danger';
        return (
            <div className="col-lg-4 col-sm-6 card-list__card" key={card.id}>
                <div className="card-list__content">
                    <h3>{card.title}</h3>
                    <div className="card-list__description">{card.description}</div>
                    <div className="card-list__buttons">
                        <div className="d-flex justify-content-between align-items-center">
                            <Link to={`${routes.EDIT_CARD_ROOT}/${card.id}`}>Edit</Link>
                            <button
                                type="button"
                                className={`btn ${isLike} btn-small`}
                                onClick={() => onLike(card.id)}>Like</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    });

    return (
        <div className="container card-list">
            <div className="row">
                <div className="col-lg-4 col-sm-6 card-list__button">
                    <div className="btn-group">
                        <Link to={routes.ADD_CARD} className="btn btn-primary">Add card</Link>
                    </div>
                </div>
                {cardsList}
            </div>
        </div>
    );
};

CardList.propTypes = {
    cards: PropTypes.array.isRequired,
    onLike: PropTypes.func.isRequired,
};

export default CardList;
