import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import AddCard from '../add-card';
import EditCard from '../edit-card';
import CardList from '../card-list';
import NotFound from '../not-found';
import CardService from '../../services/card';

import { routes } from '../../constants';
import { getNewCards } from '../../helpers';

export default class App extends Component {
    _id = 10;

    state = {
        cards: CardService.getAll(),
    };

    createCard = ({ title, description }) => ({
        id: this._id++,
        title,
        description,
        isLike: false
    });

    addCard = (card) => {
        this.setState(({ cards }) => ({ cards: [...cards, this.createCard(card)] }));
    };

    updateCard = (card) => {
        this.setState(({ cards }) => ({
            cards: getNewCards(cards, card),
        }));
    };

    deleteCard = (id) => {
        this.setState(({ cards }) => {
            const idx = cards.findIndex(el => el.id === id);
            return { cards: [...cards.slice(0, idx), ...cards.slice(idx + 1)] };
        });
    };

    like = (id) => {
        this.setState(({ cards }) => {
            const card = cards.find(c => c.id === id);
            const updatedCard = { ...card, isLike: !card.isLike };
            return { cards: getNewCards(cards, updatedCard) };
        });
    };

    render() {
        const { cards } = this.state;

        return (
            <Router>
                <div className="app container">
                    <Switch>
                        <Route
                            exact
                            path={routes.HOME_PAGE}
                            render={ () => <CardList cards={cards} onLike={this.like} /> }
                        />
                        <Route
                            exact
                            path={routes.EDIT_CARD}
                            render={ props => <EditCard
                                cards={cards}
                                onUpdateCard={this.updateCard}
                                onDeleteCard={this.deleteCard}
                                {...props}
                            /> }
                        />
                        <Route
                            exact
                            path={routes.ADD_CARD}
                            render={ props => <AddCard onAddCard={this.addCard} {...props} /> }
                        />
                        <Route component={NotFound} />
                    </Switch>
                </div>
            </Router>
        );
    }
}
