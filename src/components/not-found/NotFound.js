import React from 'react';
import { Link } from 'react-router-dom';

import { routes } from '../../constants';

const NotFound = () => (
    <div>
        <span className="badge badge-warning">404</span>
        <h2>Страница не найдена</h2>
        <Link to={routes.HOME_PAGE}>На главную</Link>
    </div>
);

export default NotFound;
