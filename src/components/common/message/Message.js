import React from 'react';
import PropTypes from 'prop-types';

const Message = (props) => {
    const { className = 'alert-warning', onClick, children } = props;
    return (
        <div className="bs-component">
            <div className={`alert alert-dismissible ${className}`}>
                <button type="button" className="close" onClick={onClick}>×</button>
                <h4 className="alert-heading">Warning!</h4>
                <p className="mb-0">{children}</p>
            </div>
        </div>
    );
};

Message.propTypes = {
    className: PropTypes.string,
    onClick: PropTypes.func,
    children: PropTypes.node,
};

export default Message;
