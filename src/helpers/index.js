const getNewCards = (cards, updateCard) => {
    const idx = cards.findIndex(el => el.id === updateCard.id);
    const oldCard = cards[idx];
    const newCard = { ...oldCard, ...updateCard };
    return [...cards.slice(0, idx), newCard, ...cards.slice(idx + 1)];
};

const isFormEmpty = ({ title, description }) => !title || !description;

export { getNewCards, isFormEmpty };
