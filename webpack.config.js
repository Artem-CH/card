require('dotenv-extended').load();

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const publicFolder = path.resolve(__dirname, 'public');
const srcFolder = path.resolve(__dirname, 'src');

const isProduction = process.env.NODE_ENV === 'production';

module.exports = {
    entry: [
        path.resolve(srcFolder, 'index.scss'),
        path.resolve(srcFolder, 'index.js'),
    ],
    output: {
        filename: isProduction ? '[hash].bundle.min.js' : '[hash].bundle.js',
        publicPath: '/',
        path: publicFolder,
    },
    resolve: {
        modules: ['src', 'node_modules'],
        extensions: ['*', '.js', '.jsx', '.json'],
        alias: {
            '@': srcFolder,
        },
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: path.resolve(__dirname, 'node_modules'),
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/react',
                        ],
                        plugins: [
                            '@babel/proposal-class-properties',
                            'react-hot-loader/babel',
                        ],
                        cacheDirectory: true,
                    }
                }
            },
            {
                test: /\.s?css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                minimize: isProduction,
                            },
                        },
                        'sass-loader',
                    ],
                })
            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            inject: true,
            template: path.resolve(srcFolder, 'index.html'),
        }),
        new ExtractTextPlugin({
            filename: isProduction ? '[hash].styles.min.css' : '[hash].styles.css',
            allChunks: true,
            disable: !isProduction,
        }),
        new webpack.HotModuleReplacementPlugin(),
    ],
    devServer: {
        host: process.env.APP_HOST,
        port: process.env.APP_PORT,
        contentBase: publicFolder,
        hot: true,
        historyApiFallback: true,
        disableHostCheck: true,
    },
};
