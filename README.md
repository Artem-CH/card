# Cards

## Installation

Create `.env` file (example in `.env.defaults`) and install dependencies:

```bash
$ cp .env.defaults .env
$ yarn install
$ yarn start
```

## Usage

Run application with follow command (run server):

```bash
$ yarn start # run dev server
$ yarn prod # run mode production
```
